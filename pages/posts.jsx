import PropTypes from 'prop-types';
import fs from 'fs';
import matter from 'gray-matter';
import path from 'path';
import Link from 'next/link';
import {
  Card,
  Grid,
  Spacer,
  Tag,
  Text,
} from '@geist-ui/core';
import { Fragment } from 'react';

import { postsDirectory } from '@/constants/blogpostData';

export default function Posts({ posts }) {
  return (
    <>
      {posts.map((singlePost, index) => (
        <Link
          href={`/${postsDirectory}/${singlePost.slug}`}
          passHref
          key={index}
        >
          <div
            style={{
              cursor: 'pointer',
            }}
          >
            <Card
              // shadow
              hoverable
              style={{
                margin: '2rem',
                transition: 'none',
              }}
            >
              <Text h4>{singlePost.frontMatter.title}</Text>
              <Text p>{singlePost.frontMatter.description}</Text>
              <Grid.Container justify="flex-end">
                <Grid marginRight="auto">
                  <Text small>{singlePost.frontMatter.date}</Text>
                </Grid>
                {singlePost.frontMatter.tags.map((element, i, array) => (i === array.length - 1 ?
                  (
                    <Grid key={i}>
                      <Tag>{element}</Tag>
                    </Grid>
                  ) :
                  (
                    <Fragment key={i}>
                      <Grid>
                        <Tag>{element}</Tag>
                      </Grid>
                      <Spacer w={0.4} />
                    </Fragment>
                  )))}
              </Grid.Container>
              <Card.Footer>Read more</Card.Footer>
            </Card>
          </div>
        </Link>
      ))}
    </>
  );
}

export const getStaticProps = async () => {
  const files = fs.readdirSync(path.join(postsDirectory));
  const posts = files.map(filename => {
    const markdownWithMeta = fs.readFileSync(
      path.join(postsDirectory, filename),
      'utf-8'
    );

    const { data: frontMatter } = matter(markdownWithMeta);

    return {
      frontMatter,
      slug: filename.split('.')[0],
    };
  });

  return {
    props: {
      posts,
    },
  };
};

Posts.propTypes = {
  posts: PropTypes.arrayOf(PropTypes.object).isRequired,
};

Posts.metaTitle = 'BlogspotUrl';
