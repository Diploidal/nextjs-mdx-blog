import PropTypes from 'prop-types';
import fs from 'fs';
import path from 'path';
import { serialize } from 'next-mdx-remote/serialize';
import { MDXRemote } from 'next-mdx-remote';
import matter from 'gray-matter';
import {
  Button,
  Card,
  Description,
  Link as GeistLink,
  Spacer,
  Text,
} from '@geist-ui/core';
import Link from 'next/link';

import { postsDirectory } from '@/constants/blogpostData';

const componentsToUseInMdx = {
  Button,
  Card,
};

export default function PostPage({
  frontMatter: {
    title,
    date,
  }, mdxSource,
}) {
  const descriptionData = {
    date,
    descTitle: 'Post created',
  };

  return (
    <>
      <Link
        href={`/${postsDirectory}`}
        passHref
      >
        <GeistLink
          block
          scale={4}
        >
          <span className="back">
            Back to list
          </span>
        </GeistLink>
      </Link>
      <Spacer h={2} />
      <Text h2>{title}</Text>
      <Description
        title={descriptionData.descTitle}
        content={(
          <small>{descriptionData.date}</small>
        )}
      />
      <Spacer h={1} />
      <MDXRemote
        {...mdxSource}
        components={componentsToUseInMdx}
      />
    </>
  );
}

export const getStaticPaths = async () => {
  const files = fs.readdirSync(path.join(postsDirectory));

  const paths = files.map(filename => ({
    params: {
      slug: filename.replace('.mdx', ''),
    },
  }));

  return {
    fallback: false,
    paths,
  };
};

export const getStaticProps = async ({ params: { slug } }) => {
  const markdownWithMeta = fs.readFileSync(
    path.join(postsDirectory, `${slug}.mdx`),
    'utf-8'
  );

  const {
    data: frontMatter, content,
  } = matter(markdownWithMeta);
  const mdxSource = await serialize(content, {
    mdxOptions: {
      useDynamicImport: true,
    },
  });

  return {
    props: {
      frontMatter,
      mdxSource,
      slug,
    },
  };
};

PostPage.propTypes = {
  frontMatter: PropTypes.shape({
    date: PropTypes.string,
    description: PropTypes.string,
    tags: PropTypes.arrayOf(PropTypes.string),
    thumbnailUrl: PropTypes.string,
    title: PropTypes.string,
  }),
  // eslint-disable-next-line react/forbid-prop-types
  mdxSource: PropTypes.any,
};
