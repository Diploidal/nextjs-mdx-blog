/* eslint-disable react/prop-types */
import Head from 'next/head';
import { useState } from 'react';
import {
  CssBaseline,
  GeistProvider,
} from '@geist-ui/core';

import {
  Footer,
  Navigation,
} from '@/core/components';
import { MainPage } from '@/core/components/Layout/Layout';

import navigationElements from '@/constants/navigationElements';

function App({
  Component,
  pageProps,
}) {
  const [
    themeType,
    setThemeType,
  ] = useState('light');

  const switchThemes = () => {
    setThemeType(last => (last === 'dark' ? 'light' : 'dark'));
  };

  const { metaTitle } = Component;

  return (
    <GeistProvider themeType={themeType}>
      <CssBaseline />
      <Head>
        <meta charSet="UTF-8" />
        <meta
          name="keywords"
          content="blog, nextjs, mdx, javascript, typescript, web"
        />
        <meta
          name="author"
          content="Maks Pryliński"
        />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1.0"
        />
        <title>{metaTitle}</title>
      </Head>
      <Navigation
        switchThemes={switchThemes}
        navigationElements={navigationElements}
      />
      <MainPage>
        <Component
          {...pageProps}
        />
      </MainPage>
      <Footer />
    </GeistProvider>
  );
}

export default App;
