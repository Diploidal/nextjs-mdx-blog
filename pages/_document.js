import Document, {
  Head,
  Html,
  Main,
  NextScript,
} from 'next/document';
import { CssBaseline } from '@geist-ui/core';

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx);
    const styles = CssBaseline.flush();

    return {
      ...initialProps,
      styles: (
        <>
          {initialProps.styles}
          {styles}
        </>
      ),
    };
  }

  render() {
    const meta = {
      description: 'Clone and deploy your own Next.js portfolio in minutes.',
      image: 'https://assets.vercel.com/image/upload/q_auto/front/vercel/dps.png',
      title: 'Next.js MDX PostList',
    };

    return (
      <Html lang="en">
        <Head>
          <meta
            name="robots"
            content="follow, index"
          />
          <meta
            name="description"
            content={meta.description}
          />
          <meta
            property="og:site_name"
            content={meta.title}
          />
          <meta
            property="og:description"
            content={meta.description}
          />
          <meta
            property="og:title"
            content={meta.title}
          />
          <meta
            property="og:image"
            content={meta.image}
          />
          <meta
            name="twitter:card"
            content="summary_large_image"
          />
          <meta
            name="twitter:site"
            content="@yourname"
          />
          <meta
            name="twitter:title"
            content={meta.title}
          />
          <meta
            name="twitter:description"
            content={meta.description}
          />
          <meta
            name="twitter:image"
            content={meta.image}
          />
          {/* TODO think about dynamic Head titles */}
          {/* <title>Next.js MDX Blog</title> */}
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default MyDocument;
