import blogpostData from './blogpostData';

const navigationElements = [
  {
    label: 'Home',
    url: '/',
  },
  blogpostData,
  {
    label: 'About',
    url: '/about',
  },
];

export default navigationElements;
