// `postsDirectory` string has to match directory with MDX files and directory with [slug].js
export const postsDirectory = 'posts';
const blogpostData = {
  label: 'Posts',
  url: `/${postsDirectory}`,
};

export default blogpostData;
