import styled from 'styled-components';

export const MainPageWrapper = styled.main`
  //TODO: basic breakpoints, fix in future and use GeistUI one (maybe?) 
  padding-top: 128px;
  @media (min-width: 1536px) {
    max-width: 1536px;
    padding-right: 6rem;
    padding-left: 6rem;
  }  
  @media (max-width: 1366px) {
    max-width: 1366px;
    padding-right: 5rem;
    padding-left: 5rem;
  }  
  @media (max-width: 1366px) {
    max-width: 1280px;
    padding-right: 5rem;
    padding-left: 5rem;
  }
  @media (max-width: 1024px) {
    max-width: 1024px;
    padding-right: 2rem;
    padding-left: 2rem;
  }
  @media (max-width: 768px) {
    max-width: 768px;
    padding-right: 2rem;
    padding-left: 2rem;
  }
  @media (max-width: 640px) {
    max-width: 640px;
    padding-right: 2rem;
    padding-left: 2rem;
  }
`;
