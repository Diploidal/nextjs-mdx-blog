import { forwardRef } from 'react';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import Link from 'next/link';
import {
  Tabs,
  useTheme,
} from '@geist-ui/core';
import Code from '@geist-ui/icons/code';

import { ThemeChangeButton } from '@/core/components';
import {
  NavigationHeader,
  NavigationWrapper,
} from '@/core/components/Navigation/Navigation.styles';

// eslint-disable-next-line react/display-name
const TabItem = forwardRef(({
  label,
  value,
// eslint-disable-next-line no-unused-vars
}, ref) => (
  <Tabs.Item
    label={label}
    value={value}
  />
));

export const Navigation = ({
  switchThemes,
  navigationElements,
}) => {
  const router = useRouter();
  const {
    palette: {
      border,
    },
  } = useTheme();

  // TODO: think about these doubled `nav` items

  return (
    <NavigationWrapper border={border}>
      <NavigationHeader>
        <Code />
        <Tabs
          hideBorder
          hideDivider
          leftSpace={0}
          value={router.asPath}
          onChange={route => router.push(route)}
        >
          {navigationElements.map(element => (
            <Link
              href={element.url}
              passHref
              key={element.label}
            >
              <TabItem
                label={element.label}
                value={element.url}
              />
            </Link>
          ))}
        </Tabs>
        <ThemeChangeButton
          className="theme-button"
          switchThemes={switchThemes}
        />
      </NavigationHeader>
    </NavigationWrapper>
  );
};

Navigation.propTypes = {
  navigationElements: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
  })).isRequired,
  switchThemes: PropTypes.func.isRequired,
};

TabItem.propTypes = {
  label: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
};
