import styled from 'styled-components';

const headerHeight = '64';

export const NavigationWrapper = styled.nav`
  position: fixed;
  top: 0;
  display: flex;
  justify-content: center;
  width: 100%;
  max-width: 100%;
  z-index: 101;
  min-height: ${headerHeight}px;
  border-bottom: 1px solid ${({ border }) => border};
  background-color: transparent;
  backdrop-filter: saturate(180%) blur(5px);
`;

export const NavigationHeader = styled.header`
  width: 1000px;
  margin: auto;
  padding: 0 128px;
  max-width: 100%;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  .content {
    //TODO: can edit content for tabs styling
    padding-top: 0 !important;
  }
`;
