import { useTheme } from '@geist-ui/core';

import {
  AnimatedBackgroundWrapper,
  FirstGradient,
  SecondGradient,
  ThirdGradient,
} from './AnimatedSC.styles';

export const AnimatedGradientBackground = () => {
  const {
    palette: {
      cyan,
      link,
      magenta,
    },
  } = useTheme();

  return (
    <AnimatedBackgroundWrapper>
      <FirstGradient color={cyan} />
      <SecondGradient color={link} />
      <ThirdGradient color={magenta} />
    </AnimatedBackgroundWrapper>
  );
};
