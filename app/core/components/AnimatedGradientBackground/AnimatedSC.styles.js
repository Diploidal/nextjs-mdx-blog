import styled, { keyframes } from 'styled-components';

const firstGradientSize = '700px';
const secondGradientSize = '600px';
const thirdGradientSize = '500px';
const gradientOpacity = '1';
const gradientAnimationDuration = '10s';
const wrapperBlurFilterAmount = '100px';

const fadeIn = keyframes`
      0% {
        opacity: 0;
      }
      100% {
        opacity: 1;
      }
    `;

const firstGradientAnimation = keyframes`
      0% {
        transform: translateY(-50%) translateX(-50%) rotate(-20deg) translateX(20%);
      }
      25% {
        transform: translateY(-50%) translateX(-50%) skew(-15deg, -15deg)
        rotate(80deg) translateX(30%);
      }
      50% {
        transform: translateY(-50%) translateX(-50%) rotate(180deg) translateX(25%);
      }
      75% {
        transform: translateY(-50%) translateX(-50%) skew(15deg, 15deg)
        rotate(240deg) translateX(15%);
      }
      to {
        transform: translateY(-50%) translateX(-50%) rotate(340deg) translateX(20%);
      }
    `;

const secondGradientAnimation = keyframes`
      0% {
        transform: translateY(-50%) translateX(-50%) rotate(40deg) translateX(-20%);
      }
      25% {
        transform: translateY(-50%) translateX(-50%) skew(15deg, 15deg)
        rotate(110deg) translateX(-5%);
      }
      50% {
        transform: translateY(-50%) translateX(-50%) rotate(210deg) translateX(-35%);
      }
      75% {
        transform: translateY(-50%) translateX(-50%) skew(-15deg, -15deg)
        rotate(300deg) translateX(-10%);
      }
      to {
        transform: translateY(-50%) translateX(-50%) rotate(400deg) translateX(-20%);
      }
    `;

const thirdGradientAnimation = keyframes`
      0% {
        transform: translateY(-50%) translateX(-50%) translateX(-15%)
        translateY(10%);
      }
      20% {
        transform: translateY(-50%) translateX(-50%) translateX(20%)
        translateY(-30%);
      }
      40% {
        transform: translateY(-50%) translateX(-50%) translateX(-25%)
        translateY(-15%);
      }
      60% {
        transform: translateY(-50%) translateX(-50%) translateX(30%) translateY(20%);
      }
      80% {
        transform: translateY(-50%) translateX(-50%) translateX(5%) translateY(35%);
      }
      to {
        transform: translateY(-50%) translateX(-50%) translateX(-15%)
        translateY(10%);
      }
    `;

export const FeaturesGradient = styled.div`
  position: absolute;
  border-radius: 100%;
  opacity: ${gradientOpacity};
  animation-iteration-count: infinite;
  animation-timing-function: cubic-bezier(0.1, 0, 0.9, 1);  
`;
export const AnimatedBackgroundWrapper = styled.div`
  //position: absolute; 
  position: fixed; /*temporary :)*/
  animation: 200ms ease-in-out ${fadeIn};
  z-index: 0;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  overflow: hidden;
  filter: blur(${wrapperBlurFilterAmount});
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;  
  pointer-events: none;
`;

export const FirstGradient = styled(FeaturesGradient)`
  background-color: ${({ color }) => color};
  width: ${firstGradientSize};
  height: ${firstGradientSize};
  z-index: -2;
  left: 60%;
  top: 40%;
  animation-duration: ${gradientAnimationDuration};  
  animation-name: ${firstGradientAnimation};
`;

export const SecondGradient = styled(FeaturesGradient)`
  background-color: ${({ color }) => color};
  width: ${secondGradientSize};
  height: ${secondGradientSize};
  z-index: -1;
  left: 40%;
  top: 60%;
  animation-duration: ${gradientAnimationDuration};
  animation-direction: reverse;  
  animation-name: ${secondGradientAnimation};
`;

export const ThirdGradient = styled(FeaturesGradient)`
  background-color: ${({ color }) => color};
  width: ${thirdGradientSize};
  height: ${thirdGradientSize};
  z-index: -3;
  left: 50%;
  top: 50%;
  animation-duration: ${gradientAnimationDuration};
  animation-name: ${thirdGradientAnimation};  
`;

