import styled from 'styled-components';

export const FooterWrapper = styled.footer`
  border-top: 1px solid ${({ border }) => border};
  padding: 12px 0;
`;

export const FooterInnerWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 0 128px;
  align-items: center;
`;
