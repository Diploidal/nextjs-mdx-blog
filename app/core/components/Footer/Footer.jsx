import {
  Text,
  useTheme,
} from '@geist-ui/core';
import Code from '@geist-ui/icons/code';

import {
  FooterInnerWrapper,
  FooterWrapper,
} from '@/core/components/Footer/Footer.styles';

export const Footer = () => {
  const {
    palette: {
      border,
    },
  } = useTheme();

  return (
    <FooterWrapper border={border}>
      <FooterInnerWrapper>
        <Text p>Copyright © 2022 Maks Pryliński.</Text>
        <Code />
      </FooterInnerWrapper>
    </FooterWrapper>
  );
};
