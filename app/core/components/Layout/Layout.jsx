import { MainPageWrapper } from '@/styles/MainPage.styles';

export const MainPage = ({ children }) => (
  <MainPageWrapper>
    {children}
  </MainPageWrapper>
);

