import PropTypes from 'prop-types';
import { useState } from 'react';
import { Button } from '@geist-ui/core';
import Sun from '@geist-ui/icons/sun';
import Moon from '@geist-ui/icons/moon';

export const ThemeChangeButton = ({
  label,
  switchThemes,
}) => {
  const [
    themeIcon,
    setThemeIcon,
  ] = useState(true);
  const toggleThemeAndIcon = () => {
    switchThemes();
    setThemeIcon(!themeIcon);
  };

  return (
    <Button
      onClick={toggleThemeAndIcon}
      iconRight={
        themeIcon ? <Sun /> : <Moon />
      }
      auto
      type="abort"
    >
      {label}
    </Button>
  );
};

ThemeChangeButton.propTypes = {
  label: PropTypes.string,
  switchThemes: PropTypes.func.isRequired,
};
